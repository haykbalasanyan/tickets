<?php

use App\Repositories\LabelRepository;
use Illuminate\Database\Seeder;

class LabelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $repository = new LabelRepository();
        $labels = [
            [
                'name' => 'Label 1',
                'color' => '#000',
            ],
            [
                'name' => 'Label 2',
                'color' => '#f0f0f0',
            ]
        ];
        foreach ($labels as $label) {
            $repository->create($label);
        }
    }
}
