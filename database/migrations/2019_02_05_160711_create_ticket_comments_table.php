<?php

use App\Models\Ticket;
use App\Models\User;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->text('comment');
            $table->unsignedInteger('author_id');
            $table->index('author_id');
            $table->foreign('author_id')
                ->references('id')->on((new User())->getTable())
                ->onDelete('cascade');
            $table->unsignedInteger('ticket_id');
            $table->index('ticket_id');
            $table->foreign('ticket_id')
                ->references('id')->on((new Ticket())->getTable())
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ticket_comments');
    }
}
