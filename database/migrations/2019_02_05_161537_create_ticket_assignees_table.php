<?php

use App\Models\Ticket;
use App\Models\User;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketAssigneesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_assignees', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('assignee_id');
            $table->index('assignee_id');
            $table->foreign('assignee_id')
                ->references('id')->on((new User())->getTable())
                ->onDelete('cascade');
            $table->unsignedInteger('ticket_id');
            $table->index('ticket_id');
            $table->foreign('ticket_id')
                ->references('id')->on((new Ticket())->getTable())
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ticket_assignees');
    }
}
