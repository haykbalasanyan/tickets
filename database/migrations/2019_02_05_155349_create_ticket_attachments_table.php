<?php

use App\Models\Ticket;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url');
            $table->integer('type');
            $table->unsignedInteger('ticket_id');
            $table->index('ticket_id');
            $table->foreign('ticket_id')
                ->references('id')->on((new Ticket())->getTable())
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ticket_attachments');
    }
}
