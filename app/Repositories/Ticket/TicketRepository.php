<?php

namespace App\Repositories\Ticket;

use App\Models\Ticket;
use App\Repositories\Repository;

class TicketRepository extends Repository
{
    public function __construct()
    {
        parent::__construct(new Ticket());
    }

    /**
     * Get ticket statuses
     *
     * @return mixed
     */
    public function statuses()
    {
        return $this->model->statuses();
    }
}