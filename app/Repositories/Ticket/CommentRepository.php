<?php

namespace App\Repositories\Ticket;

use App\Models\TicketComment;
use App\Repositories\Repository;

class CommentRepository extends Repository
{
    public function __construct()
    {
        parent::__construct(new TicketComment());
    }
}