<?php

namespace App\Repositories\Ticket;

use App\Models\TicketAssignee;
use App\Repositories\Repository;

class AssigneeRepository extends Repository
{
    public function __construct()
    {
        parent::__construct(new TicketAssignee());
    }

    public function getByAssigneeIdAndTicketId($memberId, $ticketId)
    {
        return $this->model->where(['ticket_id' => $ticketId, 'assignee_id' => $memberId])->first();
    }

    public function deleteByExceptIdsAndTicketId($ids, $ticketId)
    {
        $records = $this->model->where(['ticket_id' => $ticketId]);
        if (!count($ids)) {
            $records->delete();
        } else {
            $records->whereNotIn('id', $ids)->delete();
        }
        return true;
    }
}