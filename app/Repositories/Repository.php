<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

class Repository
{
    public $model;

    public function __construct($model)
    {
        /** @var Model model */
        $this->model = $model;
    }

    /**
     * Get record by id
     *
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return $this->model->find($id);
    }

    /**
     * Get records
     *
     * @param array $exceptIds
     * @return mixed
     */
    public function all($exceptIds = [])
    {
        $records = $this->model;
        if ($exceptIds) {
            $records = $records->whereNotIn('id', $exceptIds);
        }
        $records = $records->get();
        return $records;
    }

    /**
     * Create new record
     *
     * @param $data
     * @return Model
     */
    public function create($data)
    {
        /** @var Model $model */
        $this->model = new $this->model;
        $this->model->fill($data);
        $this->model->save();
        return $this->model;
    }

    /**
     * Edit record
     *
     * @param Model $model
     * @param $data
     * @return bool
     */
    public function edit(Model $model, $data)
    {
        $model->fill($data);
        return $model->save();
    }
}
