<?php

namespace App\Repositories;

use App\Models\Label;

class LabelRepository extends Repository
{
    public function __construct()
    {
        parent::__construct(new Label());
    }
}