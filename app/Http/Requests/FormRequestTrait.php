<?php
namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;
use Illuminate\Validation\ValidationException;

trait FormRequestTrait
{

    protected function failedValidation(Validator $validator)
    {
        $response=[
            'success'=>false,
            'data'=>[],
            'errors'=>[],
        ];
        $response['errors']['validation'] = (new ValidationException($validator->errors()))->validator;
        throw new HttpResponseException(
            Response::api($response, JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
