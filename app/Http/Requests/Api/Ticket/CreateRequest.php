<?php

namespace App\Http\Requests\Api\Ticket;

use App\Http\Requests\FormRequestTrait;
use App\Http\Requests\Request;
use App\Models\Label;
use App\Models\Ticket;

class CreateRequest extends Request
{
    use FormRequestTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $availableStatuses = "";
        foreach (Ticket::statuses() as $status) {
            $availableStatuses .= $status['id'] . ',';
        }
        return [
            'name' => 'required|max:255',
            'description' => 'required',
            'status' => 'required|in:' . $availableStatuses,
            'label_id' => 'required|exists:' . (new Label())->getTable() . ',id'
        ];
    }
}
