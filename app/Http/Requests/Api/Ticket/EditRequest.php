<?php

namespace App\Http\Requests\Api\Ticket;

use App\Http\Requests\FormRequestTrait;
use App\Http\Requests\Request;

class EditRequest extends Request
{
    use FormRequestTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'description' => 'required'
        ];
    }
}
