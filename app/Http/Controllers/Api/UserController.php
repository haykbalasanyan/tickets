<?php

namespace App\Http\Controllers\Api;

use App\Repositories\UserRepository;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserController extends Controller
{
    private $response;
    private $statusCode;
    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->response = ['success' => false, 'data' => [], 'errors' => []];
        $this->statusCode = JsonResponse::HTTP_BAD_REQUEST;
        $this->repository = $repository;
    }

    /**
     * Get current user
     *
     * @return mixed
     */
    public function me()
    {
        $this->response['data']['user'] = Auth::user();
        $this->response['success'] = true;
        $this->statusCode = JsonResponse::HTTP_OK;
        return response()->api($this->response, $this->statusCode);
    }

    /**
     * Get users
     *
     * @return mixed
     */
    public function getUsers()
    {
        $this->statusCode = JsonResponse::HTTP_OK;
        $this->response['success'] = true;
        $this->response['data']['users'] = $this->repository->all([Auth::id()]);
        return response()->api($this->response, $this->statusCode);
    }
}
