<?php

namespace App\Http\Controllers\Api\Ticket;

use App\Http\Requests\Api\Ticket\Comment\CreateRequest;
use App\Http\Controllers\Controller;
use App\Models\Ticket;
use App\Repositories\Ticket\CommentRepository;
use App\Repositories\Ticket\TicketRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    private $response;
    private $statusCode;
    private $repository;
    private $ticketRepository;

    public function __construct(CommentRepository $repository, TicketRepository $ticketRepository)
    {
        $this->response = ['success' => false, 'data' => [], 'errors' => []];
        $this->statusCode = JsonResponse::HTTP_BAD_REQUEST;
        $this->repository = $repository;
        $this->ticketRepository = $ticketRepository;
    }

    /**
     * Create
     *
     * @param Ticket $ticket
     * @param CreateRequest $request
     * @return mixed
     */
    public function create(Ticket $ticket, CreateRequest $request)
    {
        $ticket = $this->ticketRepository->find($ticket->id);
        if ($ticket) {
            $data = $request->all();
            $data['ticket_id'] = $ticket->id;
            $data['author_id'] = Auth::id();
            $comment = $this->repository->create($data);
            if ($comment) {
                $this->response['data']['comment'] = $comment->fresh(['author']);
                $this->statusCode = JsonResponse::HTTP_CREATED;
                $this->response['success'] = true;
            }
        } else {
            $this->response['errors']['message'] = 'Ticket not found';
        }
        return response()->api($this->response, $this->statusCode);
    }
}
