<?php

namespace App\Http\Controllers\Api\Ticket;

use App\Models\Ticket;
use App\Repositories\Ticket\AssigneeRepository;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AssigneeController extends Controller
{
    private $response;
    private $statusCode;
    private $repository;

    public function __construct(AssigneeRepository $repository)
    {
        $this->response = ['success' => false, 'data' => [], 'errors' => []];
        $this->statusCode = JsonResponse::HTTP_BAD_REQUEST;
        $this->repository = $repository;
    }

    /**
     * Connect user and ticket each other
     *
     * @param Request $request
     * @param Ticket $ticket
     * @return mixed
     */
    public function create(Request $request, Ticket $ticket)
    {
        $availableAssigneeIds = [];
        foreach ($request->input('assignees') as $newAssignee) {
            $assignee = $this->repository->getByAssigneeIdAndTicketId($newAssignee['value'], $ticket->id);
            if (!$assignee) {
                $data = [];
                $data['assignee_id'] = $newAssignee['value'];
                $data['ticket_id'] = $ticket->id;
                $assignee = $this->repository->create($data);
            }
            $availableAssigneeIds[$assignee->id] = $assignee->id;
        }
        $this->repository->deleteByExceptIdsAndTicketId($availableAssigneeIds, $ticket->id);
        $ticket = $ticket->fresh(['assignees']);
        $this->response['data']['assignees'] = $ticket->assignees;
        $this->statusCode = JsonResponse::HTTP_CREATED;
        $this->response['success'] = true;
        return response()->api($this->response, $this->statusCode);
    }
}
