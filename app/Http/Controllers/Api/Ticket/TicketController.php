<?php

namespace App\Http\Controllers\Api\Ticket;

use App\Http\Requests\Api\Ticket\CreateRequest;
use App\Http\Requests\Api\Ticket\EditRequest;
use App\Models\Ticket;
use App\Repositories\LabelRepository;
use App\Repositories\Ticket\TicketRepository;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class TicketController extends Controller
{
    private $response;
    private $statusCode;
    private $repository;
    private $labelsRepository;

    public function __construct(TicketRepository $repository, LabelRepository $labelsRepository)
    {
        $this->response = ['success' => false, 'data' => [], 'errors' => []];
        $this->statusCode = JsonResponse::HTTP_BAD_REQUEST;
        $this->repository = $repository;
        $this->labelsRepository = $labelsRepository;
    }

    /**
     * Get ticket statuses
     *
     * @return mixed
     */
    public function statuses()
    {
        $this->statusCode = JsonResponse::HTTP_OK;
        $this->response['success'] = true;
        $this->response['data']['statuses'] = $this->repository->statuses();
        return response()->api($this->response, $this->statusCode);
    }

    /**
     * Get labels
     *
     * @return mixed
     */
    public function labels()
    {
        $this->statusCode = JsonResponse::HTTP_OK;
        $this->response['success'] = true;
        $this->response['data']['statuses'] = $this->labelsRepository->all();
        return response()->api($this->response, $this->statusCode);
    }

    /**
     * Get tickets
     *
     * @return mixed
     */
    public function index()
    {
        $this->response['data']['success'] = true;
        $this->response['data']['tickets'] = $this->repository->all();
        $this->statusCode = JsonResponse::HTTP_OK;
        return response()->api($this->response, $this->statusCode);
    }

    /**
     * Get
     *
     * @param Ticket $ticket
     * @return mixed
     */
    public function get(Ticket $ticket)
    {
        $this->response['data']['success'] = true;
        $this->statusCode = JsonResponse::HTTP_OK;
        $this->response['data']['ticket'] = $this->repository->find($ticket->id);
        return response()->api($this->response, $this->statusCode);
    }

    /**
     * Create
     *
     * @param CreateRequest $request
     * @return mixed
     */
    public function create(CreateRequest $request)
    {
        $this->response['data']['success'] = true;
        $this->statusCode = JsonResponse::HTTP_CREATED;
        $this->response['data']['ticket'] = $this->repository->create($request->all());
        return response()->api($this->response, $this->statusCode);
    }

    /**
     * Edit
     *
     * @param EditRequest $request
     * @param Ticket $ticket
     * @return mixed
     */
    public function edit(EditRequest $request, Ticket $ticket)
    {
        $this->response['data']['success'] = true;
        $this->statusCode = JsonResponse::HTTP_CREATED;
        $this->repository->edit($ticket, $request->all());
        $this->response['data']['ticket'] = $ticket->fresh();
        return response()->api($this->response, $this->statusCode);
    }
}
