<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\Auth\RegisterRequest;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\JsonResponse;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    private $response;
    private $statusCode;
    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->response = ['success' => false, 'data' => [], 'errors' => []];
        $this->statusCode = JsonResponse::HTTP_BAD_REQUEST;
        $this->repository = $repository;
    }

    /**
     * Registration
     *
     * @param RegisterRequest $request
     * @return mixed
     */
    public function register(RegisterRequest $request)
    {
        $data = $request->all();
        $data['password'] = Hash::make($request->input('password'));
        $user = $this->repository->create($data);
        $this->response['data']['user'] = $user;
        $this->response['data']['token'] = JWTAuth::fromUser($user);
        $this->response['success'] = true;
        $this->statusCode = JsonResponse::HTTP_CREATED;
        return response()->api($this->response, $this->statusCode);
    }

    /**
     * Login
     *
     * @param Request $request
     * @return mixed
     */
    public function login(Request $request)
    {
        $this->response['errors']['validation']['auth'] = 'Wrong E-mail or Password';
        $this->statusCode = JsonResponse::HTTP_UNAUTHORIZED;
        if (JWTAuth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
            $user = Auth::user();
            $this->response['data']['user'] = $user;
            $this->response['data']['token'] = JWTAuth::fromUser($user);
            $this->response['success'] = true;
            $this->statusCode = JsonResponse::HTTP_OK;
        }
        return response()->api($this->response, $this->statusCode);
    }
}
