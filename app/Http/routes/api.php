<?php

use Illuminate\Support\Facades\Route;

// Authorization
Route::group(['prefix' => 'auth'], function () {
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
});

// Ticket
Route::get('tickets', 'Ticket\TicketController@index');
Route::group(['prefix' => 'ticket'], function () {
    Route::get('statuses', 'Ticket\TicketController@statuses');
    Route::get('labels', 'Ticket\TicketController@labels');
    Route::post('create', 'Ticket\TicketController@create');
    Route::group(['prefix' => '{ticket}'], function () {
        Route::get('/', 'Ticket\TicketController@get');
        Route::patch('/', 'Ticket\TicketController@edit');
    });
});

Route::group(['middleware' => 'jwt.auth'], function () {
    // Users
    Route::get('users', 'UserController@getUsers');

    Route::group(['prefix' => 'user'], function () {
        Route::get('me', 'UserController@me');
    });
    Route::group(['prefix' => 'ticket'], function () {
        Route::group(['prefix' => '{ticket}'], function () {
            Route::group(['prefix' => 'comment', 'middleware'], function () {
                Route::post('/', 'Ticket\CommentController@create');
            });
            Route::group(['prefix' => 'assignee', 'middleware'], function () {
                Route::post('/', 'Ticket\AssigneeController@create');
            });
        });
    });
});