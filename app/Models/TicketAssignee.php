<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketAssignee extends Model
{
    protected $table = "ticket_assignees";

    protected $with = ["user"];

    protected $fillable=['assignee_id','ticket_id'];

    /**
     * Get user data
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'assignee_id');
    }
}
