<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketComment extends Model
{
    protected $table = "ticket_comments";

    protected $fillable = ['text', 'ticket_id', 'author_id'];

    protected $with=['author'];

    /**
     * Get comment author data
     */
    public function author()
    {
        return $this->hasOne(User::class, 'id', 'author_id');
    }
}
