<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = "projects";

    protected $fillable = ['name'];

    //Board types
    const BOARD_KANBAN = 1;
    const BOARD_SCRUM = 2;
    const BOARD_BUG_TRACKING = 3;
}
