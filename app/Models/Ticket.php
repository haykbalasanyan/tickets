<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = "tickets";

    protected $fillable = ['name', 'description', 'status', 'label_id'];

    protected $with = ['assignees', 'comments', 'attachments'];

    /**
     * Ticket statuses
     *
     * @return array
     */
    public static function statuses()
    {
        return [
            ['id' => 1, 'name' => 'Task'],
            ['id' => 2, 'name' => 'Bug'],
        ];
    }

    /**
     * Get assignees
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function assignees()
    {
        return $this->hasMany(TicketAssignee::class, 'ticket_id', 'id');
    }

    /**
     * Get comments
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(TicketComment::class, 'ticket_id', 'id');
    }

    /**
     * Get attachments
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attachments()
    {
        return $this->hasMany(TicketAttachment::class, 'ticket_id', 'id');
    }
}
