import React, {Component} from 'react';
import {Provider} from '../../components/layouts/Container'

class Container extends Component {
    constructor(props) {
        super(props);
        this.state = {
            something: 'hey'
        }
    }

    render() {
        return (
            <Provider value={{state: this.state}}>{this.props.children}</Provider>
        )
    }
}