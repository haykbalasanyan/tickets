import React, {Component} from 'react';
import api from "../../services/api";

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {}
        };
    }

    componentDidMount() {
        if (!this.state.user.name) {
            if (localStorage.getItem('ticket_token')) {
                api(localStorage.getItem('ticket_token')).get('/user/me').then((res) => {
                    this.setState({user: res.data.data.user});
                }).catch((error) => {
                    console.log(error);
                })
            }
        }
    }

    componentWillUpdate(nextProps, nextState, nextContext) {
        if (!this.state.user.name) {
            if (localStorage.getItem('ticket_token')) {
                api(localStorage.getItem('ticket_token')).get('/user/me').then((res) => {
                    this.setState({user: res.data.data.user});
                    console.log(this.state.name);
                }).catch((error) => {
                    console.log(error);
                })
            }
        }
    }

    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <a className="navbar-brand" href="#">Ticket</a>

                <div className="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul className="navbar-nav">
                        <li className="nav-item active">
                            <a className="nav-link"
                               href="javascript:void(0)">Hello {this.state.user ? this.state.user.name : ""}</a>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}

export default Header;