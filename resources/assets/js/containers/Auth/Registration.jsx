import React, {Component} from 'react';
import api from "../../services/api";
import Header from "../../components/layouts/HeaderComponent";

class Registration extends Component {
    constructor(props) {
        super(props);
        this.state = {
            form: {
                name: null,
                email: null,
                password: null,
                password_confirmation: null,
            },
            errors: []
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.hasError = this.hasError.bind(this);
        this.getError = this.getError.bind(this);
    }


    handleChange(e) {
        let inputName = e.target.name;
        this.state.form[inputName] = e.target.value;
        this.setState({form: this.state.form});
    }

    handleSubmit(e) {
        e.preventDefault();
        api().post('auth/register', this.state.form).then((res) => {
            localStorage.setItem('ticket_token', res.data.data.token);
            this.props.history.push('/tickets');
        }).catch((errors) => {
            this.setState({errors: errors.response.data.errors.validation})
        });
        return false;
    };


    hasError(name) {
        let has = false;
        if (this.state.errors[name] !== undefined) {
            has = true;
        }
        return has;
    };


    getError(name) {
        let message = null;
        if (this.state.errors[name] !== undefined) {
            message = this.state.errors[name][0];
        }
        return message;
    };


    render() {
        return (
            <div>
                <Header user={this.state.user}/>
                <div id="registrationBlock">
                    <h4>Registration</h4>
                    <form onSubmit={this.handleSubmit}>
                        <div className={'form-group'}>
                            <label className={'control-label'}>Name</label>
                            <input type="text" className={'form-control ' + (this.hasError('name') ? 'is-invalid' : "")}
                                   name="name" onChange={this.handleChange}/>
                            <div className="invalid-feedback">
                                {this.getError('name')}
                            </div>
                        </div>
                        <div className="form-group">
                            <label className="control-label">E-mail</label>
                            <input type="text" name="email"
                                   className={'form-control ' + (this.hasError('email') ? 'is-invalid' : "")}
                                   onChange={this.handleChange}/>
                            <div className="invalid-feedback">
                                {this.getError('email')}
                            </div>
                        </div>
                        <div className="form-group">
                            <label className="control-label">Password</label>
                            <input type="password" name="password"
                                   className={'form-control ' + (this.hasError('password') ? 'is-invalid' : "")}
                                   onChange={this.handleChange}/>
                            <div className="invalid-feedback">
                                {this.getError('password')}
                            </div>
                        </div>
                        <div className="form-group">
                            <label className="control-label">Password Confirmation</label>
                            <input type="password" name="password_confirmation"
                                   className={'form-control ' + (this.hasError('password_confirmation') ? 'is-invalid' : "")}
                                   onChange={this.handleChange}/>
                            <div className="invalid-feedback">
                                {this.getError('password_confirmation')}
                            </div>
                        </div>
                        <button type="submit" className={'btn btn-primary'}>Register</button>
                    </form>
                </div>
            </div>
        )
    }
}

export default Registration;