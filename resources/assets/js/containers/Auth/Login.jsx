import React, {Component} from 'react';
import api from "../../services/api";
import Header from "../../components/layouts/HeaderComponent";
import {Link} from "react-router-dom";

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            form: {
                email: null,
                password: null
            },
            errors: []
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.hasError = this.hasError.bind(this);
        this.getError = this.getError.bind(this);
    }


    handleChange(e) {
        let inputName = e.target.name;
        this.state.form[inputName] = e.target.value;
        this.setState({form: this.state.form});
    }

    handleSubmit(e) {
        e.preventDefault();
        this.setState({errors: []});
        api().post('auth/login', this.state.form).then((res) => {
            localStorage.setItem('ticket_token', res.data.data.token);
            this.props.history.push('/tickets');
        }).catch((errors) => {
            this.setState({errors: errors.response.data.errors.validation});
        });
        return false;
    };


    hasError(name) {
        let has = false;
        if (this.state.errors[name] !== undefined) {
            has = true;
        }
        return has;
    };


    getError(name) {
        let message = null;
        if (this.state.errors[name] !== undefined) {
            message = this.state.errors[name];
        }
        return message;
    };


    render() {
        return (
            <div>
                <Header user={this.state.user}/>
                <div id="loginBlock">
                    {this.getError('auth') ? (<div className="alert alert-danger" role="alert">
                        {this.getError('auth')}
                    </div>) : ""}
                    <h4>Login</h4>
                    <form onSubmit={this.handleSubmit}>
                        <div className="form-group">
                            <label className="control-label">E-mail</label>
                            <input type="text" name="email"
                                   className={'form-control'}
                                   onChange={this.handleChange}/>
                        </div>
                        <div className="form-group">
                            <label className="control-label">Password</label>
                            <input type="password" name="password"
                                   className={'form-control'}
                                   onChange={this.handleChange}/>
                        </div>
                        <Link to="/auth/register" className="float-right">Register</Link>
                        <button type="submit" className={'btn btn-primary'}>Login</button>
                    </form>
                </div>
            </div>
        )
    }
}

export default Login;