import React, {Component} from 'react';
import api from "../../services/api";
import Header from "../../components/layouts/HeaderComponent";
import {Link} from "react-router-dom";

export default class List extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tickets: []
        };
        this.getTickets();
    }

    getTickets() {
        api().get('tickets').then((res) => {
            this.setState({tickets: res.data.data.tickets});
        });
    }

    render() {
        return (
            <div id="tickets">
                <Header/>
                {this.state.tickets.map((item, index) => (
                    <div className="card ticket" key={item.id}>
                        <img src="https://dummyimage.com/150.png/09f/fff" className="card-img-top" alt="..."/>
                        <div className="card-body">
                            <h5 className="card-title">{item.name}</h5>
                            <p className="card-text">{item.description}</p>
                            <Link to={"/ticket/" + item.id} className={'btn btn-primary'}>Edit</Link>
                        </div>
                    </div>
                ))}
            </div>
        );
    }
}