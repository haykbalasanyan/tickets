import React, {Component} from 'react';
import api from "../../services/api";
import Header from "../../components/layouts/HeaderComponent";
import Select from 'react-select';

export default class Detail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ticket: {},
            id: this.props.match.params.id,
            form: {
                text: null,
                description: null,
                name: null,
            },
            wantEditName: false,
            wantEditDescription: false,
            users: [],
            errors: [],
            selectedAssignees: null
        };
        this.getTicket();
        this.getUsers();
        this.handleChange = this.handleChange.bind(this);
        this.addComment = this.addComment.bind(this);
        this.handleAssigneesChange = this.handleAssigneesChange.bind(this);
        this.openWantEditDescription = this.openWantEditDescription.bind(this);
        this.openWantEditName = this.openWantEditName.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    getTicket() {
        api().get('ticket/' + this.state.id).then((res) => {
            this.setState({ticket: res.data.data.ticket});
            this.state.form.name = this.state.ticket.name;
            this.state.form.description = this.state.ticket.description;
            this.setState({form: this.state.form});
            let assignees = [];
            this.state.ticket.assignees.forEach((item, index) => {
                assignees.push({value: item.user.id, label: item.user.name});
            });
            this.setState({selectedAssignees: assignees});
        });
    }

    addComment(e) {
        e.preventDefault();
        api(localStorage.getItem('ticket_token')).post('ticket/' + this.state.id + '/comment', this.state.form).then((res) => {
            let comment = res.data.data.comment;
            this.state.ticket['comments'].unshift(comment);
            this.setState({ticket: this.state.ticket});
            this.refs.comment.value = "";
        }).catch((errors) => {
            this.setState({errors: errors.response.data.errors.validation});
        });
    }

    getUsers() {
        api(localStorage.getItem('ticket_token')).get('/users').then((res) => {
            Object.keys(res.data.data.users).forEach((index) => {
                let user = res.data.data.users[index];
                this.state.users.push({value: user.id, label: user.name});
                this.setState({users: this.state.users});
            });
        });
    }

    hasError(name) {
        let has = false;
        if (this.state.errors[name] !== undefined) {
            has = true;
        }
        return has;
    };


    getError(name) {
        let message = null;
        if (this.state.errors[name] !== undefined) {
            message = this.state.errors[name];
        }
        return message;
    };

    handleChange(e) {
        let inputName = e.target.name;
        this.state.form[inputName] = e.target.value;
        this.setState({form: this.state.form});
    }

    handleAssigneesChange(selectedAssignees) {
        api(localStorage.getItem('ticket_token')).post('/ticket/' + this.state.id + '/assignee', {assignees: selectedAssignees}).then((res) => {
            this.state.ticket.assignees = res.data.data.assignees;
            this.setState({ticket: this.state.ticket});
            let assignees = [];
            this.state.ticket.assignees.forEach((item, index) => {
                assignees.push({value: item.user.id, label: item.user.name});
            });
            this.setState({selectedAssignees: assignees});
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        api().patch('ticket/' + this.state.id, this.state.form).then((res) => {
            this.setState({ticket: res.data.data.ticket, wantEditName: false, wantEditDescription: false});
        }).catch((errors) => {
            this.setState({errors: errors.response.data.errors.validation})
        });
        return false;
    }

    openWantEditName() {
        this.setState({wantEditDescription: false});
        this.setState({wantEditName: true});
    }

    openWantEditDescription() {
        this.setState({wantEditDescription: true});
        this.setState({wantEditName: false});
    }

    render() {
        const {selectedAssignees} = this.state;
        return (
            <div id="ticket">
                <Header/>
                <form onSubmit={this.handleSubmit}>
                    {!this.state.wantEditName ? (
                        <h1 onClick={this.openWantEditName}>{this.state.ticket.name}</h1>) : (
                        <div className='form-group title-change-block'>
                            <input type='text' value={this.state.form.name} name="name"
                                   className={'form-control ' + (this.hasError('name') ? 'is-invalid' : "")}
                                   onChange={this.handleChange}/>
                            <button type="submit" className="btn btn-primary save-btn">Save</button>
                            <div className="invalid-feedback">
                                {this.getError('name')}
                            </div>
                        </div>)}
                    {!this.state.wantEditDescription ? (
                        <p onClick={this.openWantEditDescription}>{this.state.ticket.description}</p>) : (
                        <div className='form-group description-change-block'>
                            <textarea
                                className={'form-control ' + (this.hasError('description') ? 'is-invalid' : "")}
                                defaultValue={this.state.form.description} name='description'
                                onChange={this.handleChange}></textarea>
                            <button type="submit" className="btn btn-primary save-btn">Save</button>
                            <div className="invalid-feedback">
                                {this.getError('description')}
                            </div>
                        </div>)}
                    {this.state.users.length ? <Select
                        value={selectedAssignees}
                        options={this.state.users}
                        onChange={this.handleAssigneesChange}
                        isMulti="true"
                    /> : false}
                    <br/>
                </form>
                <b>Members</b>
                {this.state.ticket.assignees ? this.state.ticket.assignees.map((item, index) => (
                    <div className="comment" key={item.id}>
                        <p>{item.user.name}</p>
                    </div>
                )) : false}
                <div className="comments">
                    <form onSubmit={this.addComment}>
                        <div className="form-group">
                            <label htmlFor="comment" className="control-label">
                                <b>Comment</b>
                            </label>
                            <textarea className={"form-control " + (this.hasError('text') ? 'is-invalid' : "")}
                                      placeholder="Comment" id="comment" name="text"
                                      onChange={this.handleChange} ref="comment"></textarea>
                            <div className="invalid-feedback">
                                {this.getError('text')}
                            </div>
                        </div>
                        <button type="submit" className="btn btn-primary float-right">Add</button>
                    </form>
                    {this.state.ticket.comments ? this.state.ticket.comments.map((item, index) => (
                        <div className="comment" key={item.id}>
                            <p><b>{item.author.name}</b>-{item.text}</p>
                        </div>
                    )) : false}
                </div>
            </div>
        );
    }
}