import React, {Component} from 'react';
import {Route, Switch, HashRouter} from "react-router-dom";
import Registration from "./Auth/Registration";
import Login from "./Auth/Login";
import List from "./Ticket/List";
import Detail from "./Ticket/Detail";

class App extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <HashRouter>
                <div className="content">
                    <Switch>
                        <Route path="/auth/register" exact={true} component={Registration}/>
                        <Route path="/" exact={true} component={Login}/>
                        <Route path="/tickets" exact={true} component={List}/>
                        <Route path="/ticket/:id" exact={true} component={Detail}/>
                    </Switch>
                </div>
            </HashRouter>
        );
    }
}

export default App;