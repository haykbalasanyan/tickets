import axios from 'axios';

let config = {
    baseURL: window.location.origin + '/api'
};
export default (token = null) => {
    if (token) {
        config.headers = {};
        config.headers['Authorization'] = 'Bearer ' + token;
    }
    return axios.create(config);
}