<!DOCTYPE html>
<html>
<head>
    <title>Laravel</title>
    <link rel="stylesheet" href="{{asset('/assets/css/app.css')}}">
</head>
<body>
<div id="app">
</div>
<script src="/assets/js/app.js"></script>
</body>
</html>
